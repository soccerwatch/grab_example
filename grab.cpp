#include <iostream>
#include <fcntl.h>
#include "unistd.h"
#include <sys/ioctl.h>
#include <linux/videodev2.h>
#include <vector>
#include <memory>
#include <cstring>
#include <cuda_runtime.h>
#ifdef WITH_CUDA
    #include "cuda.h"
#endif

static size_t NUM_CAMS=6;
const size_t NUM_BUFFERS=5;
struct Device
{
    int fd;
    size_t id;
    unsigned int image_size = 0;
    size_t width = 0;
    size_t height = 0;
    size_t step = 0;
    Device(int fd, size_t id): fd(fd),id(id){}
};
typedef std::vector<Device> Devices;
struct Buffer
{
    std::vector<v4l2_buffer> v4l2Buffers;
    int index;
    char* data;
};

typedef std::vector<Buffer> Buffers;


Devices findNumCams()
{
    Devices devices;
    for(size_t i=0; i< NUM_CAMS; i++)
    {
        int fd;
        if((fd = open(("/dev/video"+std::to_string(i)).c_str(), O_RDWR)) >= 0)
        {
            devices.push_back(Device(fd, i));
        }
    }
    NUM_CAMS = devices.size();
    return devices;
}

Buffers allocBuffers(Devices devices)
{
    for (size_t i=0; i< devices.size();i++)
    {

        struct v4l2_requestbuffers rb;
        memset(&rb, 0, sizeof rb);

        rb.count = static_cast<unsigned int>(NUM_BUFFERS);
        rb.type = V4L2_CAP_VIDEO_CAPTURE;
        rb.memory = V4L2_MEMORY_USERPTR;
        int ret = ioctl(devices[i].fd, VIDIOC_REQBUFS, &rb);
        if (ret < 0) {
            printf("Unable to request buffers:");
        }
    }

    Buffers buffers(NUM_BUFFERS);
    int index=0;
    for (Buffer& buffer : buffers)
    {
        buffer.v4l2Buffers = std::vector<v4l2_buffer>(devices.size());
        buffer.index = index++;
        size_t numBuffer = 0;
        for (v4l2_buffer& buffer_v4l2 : buffer.v4l2Buffers)
        { 
            memset(&buffer_v4l2, 0, sizeof buffer_v4l2);
            buffer_v4l2.type = V4L2_CAP_VIDEO_CAPTURE;
            buffer_v4l2.memory = V4L2_MEMORY_USERPTR;
            buffer_v4l2.index = static_cast<unsigned int>(buffer.index);
            buffer_v4l2.length = devices[0].image_size;

            int ret = ioctl(devices[numBuffer++].fd, VIDIOC_QUERYBUF, &buffer_v4l2);
            if (ret < 0) {
                printf("Unable to query buffer");
            }
        }
    }
    return buffers;
}

void queueBuffer(Devices& devices, Buffer &buffer)
{

    cudaMallocManaged(&buffer.data, devices[0].image_size * NUM_CAMS);

    for (size_t i=0; i < buffer.v4l2Buffers.size();i++)
    {
        v4l2_buffer& b = buffer.v4l2Buffers[i];
        b.m.userptr = reinterpret_cast<long unsigned>(buffer.data + devices[i].id * b.length);
        int ret = ioctl(devices[i].fd, VIDIOC_QBUF, &b);
        if (ret<0)
        {
            printf("Queue buffer failed: ");
        }
    }
}


Buffer& dequeueBuffer(Devices& devices, Buffers& buffers, size_t current_index)
{
    Buffer& buffer = buffers[current_index];

    for (size_t b=0; b< buffer.v4l2Buffers.size(); b++)
    {
        v4l2_buffer& v4l2buffer = buffer.v4l2Buffers[b];
        int ret = ioctl(devices[b].fd, VIDIOC_DQBUF, &v4l2buffer);
        if (ret<0)
        {
            printf("DQUEUE Buffer failed");
            exit(-1);
        }
    }

    return buffer;
}


void streamEnabled(Devices& devices, bool value)
{
    for(size_t d=0; d < devices.size(); d++)
    {
        Device& dev  = devices[d];
        auto type =V4L2_CAP_VIDEO_CAPTURE;
        int ret = ioctl(dev.fd, value ? VIDIOC_STREAMON : VIDIOC_STREAMOFF, &type);
        if (ret <0)
            printf("Enabling/Disabling stream failed");
    }
}

void initCams(Devices& devices, int width, int height)
{   unsigned int v4l2format;
    for (Device& dev : devices)
    {

        struct v4l2_format fmt;

        memset(&fmt, 0, sizeof fmt);
        fmt.type = V4L2_CAP_VIDEO_CAPTURE;
        fmt.fmt.pix.width = static_cast<unsigned int>(width);
        fmt.fmt.pix.height = static_cast<unsigned int>(height);
        fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_UYVY;
        fmt.fmt.pix.field = V4L2_FIELD_NONE;
        fmt.fmt.pix.bytesperline = 0;
        fmt.fmt.pix.sizeimage = 0;
        fmt.fmt.pix.priv = V4L2_PIX_FMT_PRIV_MAGIC;
        fmt.fmt.pix.flags = 0;
        std::cout << dev.fd << " " << dev.id << " " << width << " " << height << std::endl;

        int ret = ioctl(dev.fd, VIDIOC_S_FMT, &fmt);
        if (ret != 0) {
            printf("Unable to set format: %s (%d).\n", strerror(errno), errno);
        }
        dev.image_size = fmt.fmt.pix.sizeimage;
        dev.width = fmt.fmt.pix.width;
        dev.height = fmt.fmt.pix.height;
        dev.step = fmt.fmt.pix.bytesperline;

        v4l2format=fmt.fmt.pix.pixelformat;
        //Setting up framerate
        struct v4l2_streamparm streamparm;
        memset(&streamparm, 0, sizeof(streamparm));
        streamparm.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

    }
}

void process(Devices& devices, int num_frames= 10000)
{
    int first_frame = true;
    size_t current_index = 0;

    Buffers buffers = allocBuffers(devices);
    streamEnabled(devices, true);
    for(Buffer& buffer : buffers)
        queueBuffer(devices, buffer);
    first_frame=false;
    for (int i=0; i< num_frames; i++)
    {
        Buffer& buffer = dequeueBuffer(devices, buffers,current_index);

        char* data = buffer.data;
        // Do something with the frames

        queueBuffer(devices, buffer);
        
        cudaFree(data);
         	

        printf("frame %d done\n",i);
        current_index++;
        current_index %= NUM_BUFFERS;
    }
    streamEnabled(devices, false);
}

int main()
{
    int width = 1280;
    int height = 720;
    Devices devs = findNumCams();
    initCams(devs, width, height);
    process(devs);
    for (Device fd : devs)
        close(fd.fd);
}
